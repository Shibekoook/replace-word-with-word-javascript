const result_inp = document.getElementById('result');
const form = document.getElementById('form');
const copyBtn = document.getElementById('copyBtn');
const cutBtn = document.getElementById('cutBtn');

form.addEventListener('submit', (e) => {
  e.preventDefault();
  const text = document.getElementById('text').value;
  const word_to_replace = document.getElementById('word-to-replace').value;
  const word_to_replace_with = document.getElementById('word-to-replace-with').value;
  const resultText = text.replaceAll(word_to_replace, word_to_replace_with);
  result_inp.innerText = resultText;
});

copyBtn.addEventListener('click', () => {
  result_inp.select();
  document.execCommand('copy');
});

// cutBtn.addEventListener('click', () => {
//   result_inp.select();
//   document.execCommand('cut');
// });

// function copyResult() {
//   result_inp.select();
//   document.execCommand('copy');
// }